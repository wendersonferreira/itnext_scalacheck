name := "itnext_scalacheck"

val version = "0.1.0-ALPHA.1"

scalaVersion := "2.10.7"

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.3" % Test
