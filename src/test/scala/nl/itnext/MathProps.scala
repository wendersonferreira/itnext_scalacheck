package nl.itnext

import org.scalacheck.{Prop, Properties}

object MathProps extends Properties("MathProps") {
  property("max") = Prop.forAll { (x: Int, y: Int) =>
    val result = Math.max(x, y)
    (result == x || result == y) && (result >= x || result >= y)
  }
}
